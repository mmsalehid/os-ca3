#include <semaphore.h>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>

using namespace std;
using namespace std::chrono;

long totalEmission = 0;
sem_t tMutex;
struct Path{
    sem_t mutex;
    char start;
    char end;
    int h;
    Path(char _start, char _end, char _h){
        start = _start;
        end = _end;
        h = _h - 48;
        sem_init(&mutex,0,1);
    }
};

struct CarDataInPath{
    Path* path;
    int carId;
    int pathId;
    long emission;
    long savedTotalEmission;
    milliseconds startTime;
    milliseconds finishTime;
    CarDataInPath(Path* _path, int _carId, int _pathId, long _emission, long _tEmission, milliseconds _start, milliseconds _finish)
    :path(_path), carId(_carId), pathId(_pathId), emission(_emission), 
     savedTotalEmission(_tEmission), startTime(_start), finishTime(_finish)
    {}
    
};

class Car{
private:
    vector<Path*> pathes;
    int p;
    int pathIndex;
    int pathId;
    int carId;
    vector<CarDataInPath*> data;
public:
    Car(vector<Path*> _pathes, int _pathId, int _carId){
        p = rand()%10 + 1;
        this->pathes = _pathes; 
        pathId = _pathId;
        carId = _carId;
        pathIndex = 0;
    }
    void wait(){
        sem_wait(&this->pathes[pathIndex]->mutex);
    }
    void signal(){
        sem_post(&this->pathes[pathIndex]->mutex);
    }

    long calculatePolution(){
        long sum = 0;
        for(int i = 0; i < 10000000;i++){
            sum += i/(1000000*p*pathes[pathIndex]->h);
        }
        // cout << sum << endl;
        return sum;
    }
    void move(){
        for(int i = 0; i < pathes.size(); i++){
            pathIndex = i;
            this->travelPath();
        }
        this->saveAllDataToCsv();
    }
    void travelPath(){
        milliseconds startTime = duration_cast< milliseconds >(
            system_clock::now().time_since_epoch()
        );
        wait();
        long polution =  calculatePolution();
        signal();
        milliseconds finishTime = duration_cast< milliseconds >(
            system_clock::now().time_since_epoch()
        );
        long totalPolution = calculateTotalEmission(polution);
        saveData(polution, totalPolution, startTime, finishTime);
    }
    long calculateTotalEmission(long p){
        sem_wait(&tMutex);
        totalEmission += p;
        long result = totalEmission;
        sem_post(&tMutex);
        return result;
    }
    void saveDataToCsv(int dataIndex,ofstream* carFile){
        CarDataInPath* nodeData = data[dataIndex];
        *carFile << nodeData->path->start << "," <<nodeData->startTime.count() <<  "," << nodeData->path->end << ",";
        *carFile << nodeData->finishTime.count() << ","  << nodeData->emission<< "," << nodeData->savedTotalEmission << endl;
    }
    void saveAllDataToCsv(){
        string fileName = to_string(pathId) + "-" + to_string(carId);
        ofstream carFile(fileName);
        for(int i = 0; i < data.size(); i++){
            saveDataToCsv(i, &carFile);
        }
        carFile.close();
    }
    void saveData(long poloution,long totalPloution,milliseconds startTime, milliseconds finishTime){
        CarDataInPath *newData = new CarDataInPath(pathes[pathIndex], carId, pathId, poloution, totalPloution, startTime, finishTime);
        data.push_back(newData);
    }
};

void threadFunction(Car* car){
    car->move();
}

int main(int argc, char** argv){
    vector<Path*> pathes;
    vector<Car*> cars;
    vector<thread*> threads;
    string line = " ";
    ifstream inputFile(argv[1]);
    sem_init(&tMutex, 0, 1);
    while(line != "#"){
        getline(inputFile,line);
        Path* newPath = new Path(line[0], line[4], line[8]);
        pathes.push_back(newPath);
    }
    int numOfCars;
    int pathCounter = 0;
    int carCounter = 0;
    while(getline(inputFile, line)){
        string carNum;
        getline(inputFile, carNum); 
        numOfCars = atoi(carNum.c_str());
        vector<Path*> carPathes;
        for(int i = 0; i < line.size()-2; i+=4){
            Path* path;
            for(int j = 0; j < pathes.size(); j++){
                if(pathes[j]->start == line[i] && pathes[j]->end == line[i+4]){
                    path = pathes[j];
                }
            }
            carPathes.push_back(path);
        }
        for(int i = 0; i < numOfCars; i++){
            Car* newCar =  new Car(carPathes, pathCounter, carCounter);
            cars.push_back(newCar);
            carCounter++;
        }
        pathCounter++;
    }
    for (int i = 0; i < cars.size(); i++){
        thread* newThread = new thread(threadFunction, cars[i]);
        threads.push_back(newThread);
    }
    for (int i = 0; i < threads.size(); i++){
        threads[i]->join();
    }

}